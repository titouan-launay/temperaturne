var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
if(process.env.OPENSHIFT == "true") {
    console.log("Connecting to openshift data");
    var sequelize = new Sequelize('sampledb', 'user07Q', 'pYwRCfHLlBTbmROm', {
        host: 'postgresql',
        port: 5432,
        dialect: 'postgres'
    });
    sequelize.sync()
} else {
    console.log("Connecting to local data");
    var sequelize = new Sequelize('temperaturne', 'postgres', 'root', {
        dialect: 'postgres'
    });
    sequelize.sync()
}
const Op = Sequelize.Op;

const temperature = sequelize.define('temperature', {
    temperature: Sequelize.INTEGER
})


/* GET users listing. */
router.get('/measure', function(req, res, next) {
    var pin = temperature.create({
        temperature: parseInt(req.query.d)
    }).then(() => {
        res.send("OK");
    })
});

router.get('/measures', function(req, res, next) {
    temperature.findAll({
        where: {
            createdAt: {
                [Op.gt]: new Date(new Date() - 24 * 60 * 60 * 1000)
            }
        }
    }).then((rows) => {
        var answer = [];
        rows.map((row) => {
            answer.push({
                id: row.id,
                createdAt: row.createdAt,
                temperature: row.temperature
            })
        })
        res.json(answer)
    })
})

module.exports = router;
